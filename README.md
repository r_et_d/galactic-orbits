# galactic-orbits

## Table of contents for galactic orbit software and studies

* [GALORB (GALactic ORBit simulations)](https://bitbucket.org/r_et_d/galorb/wiki/Home) - User documentation and usage instructions
* [GALORB code](https://bitbucket.org/r_et_d/galorb/src/master/) - a Python tool to provide a user friendly command line interface for orbit simulations